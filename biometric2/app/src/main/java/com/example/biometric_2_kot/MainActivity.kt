package com.example.biometric_2_kot

import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricPrompt
import androidx.core.app.ActivityCompat
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CancellationSignal
import android.widget.Toast
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.Executor

class MainActivity : AppCompatActivity() {

    private var cancellationSignal: CancellationSignal? = null
    private val authenticationCallback: (Bundle?) -> Unit
    get() =
        @RequiresApi(Build.VERSION_CODES.P)
        object : BiometricPrompt.AuthenticationCallback(), (Bundle?) -> Unit {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                super.onAuthenticationError(errorCode, errString)
                notifyUser("Autenticaon error: $errString")
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
                super.onAuthenticationSucceeded(result)
                notifyUser("Autenticaon success")
                startActivity(Intent(this@MainActivity,SecretActivity::class.java))
            }

            override fun invoke(p1: Bundle?) {
                TODO("Not yet implemented")
            }
        }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkBiometricSupport()

        btn_authenticate.setOnClickListener {
            val biometricPrompt=BiometricPrompt.Builder(this)
                .setTitle("Titutlo del propm")
                .setSubtitle("Autenticacion requerida")
                .setDescription("Uso del fingerprint biometric")
                .setNegativeButton("Cancel",this.mainExecutor,DialogInterface.OnClickListener{dialog,wich ->
                    notifyUser("Autenticacion cancelada")
                })

            biometricPrompt.authenticate(getCancellationSignal(),mainExecutor,authenticationCallback)
        }

    }

    private fun getCancellationSignal(): CancellationSignal{
        cancellationSignal = CancellationSignal()
        cancellationSignal?.setOnCancelListener {
            notifyUser("Autenticacion cancelada por el usuario")
        }

        return cancellationSignal as CancellationSignal

    }

    private fun checkBiometricSupport(): Boolean {
        val keyguardManager: KeyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

        if (!keyguardManager.isKeyguardSecure){
            notifyUser("No esta activado fingerprint en los ajustes")
            return false
        }

        if (ActivityCompat.checkSelfPermission(this,android.Manifest.permission.USE_BIOMETRIC) != PackageManager.PERMISSION_GRANTED){
            notifyUser("El permiso no esta disponile")
            return false
        }
        return if (packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)){
            true
        }else true
    }

    private fun notifyUser(message : String){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }
}

private fun Any.authenticate(cancellationSignal: CancellationSignal, mainExecutor: Executor?, authenticationCallback: (Bundle?) -> Unit) {

}

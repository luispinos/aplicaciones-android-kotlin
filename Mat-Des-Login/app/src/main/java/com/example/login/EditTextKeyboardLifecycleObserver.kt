package com.example.login

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import java.lang.ref.WeakReference

class EditTextKeyboardLifecycleObserver(
    private val editText: WeakReference<EditText>
) :
    LifecycleObserver {

    @OnLifecycleEvent(
        Lifecycle.Event.ON_RESUME
    )
    fun openKeyboard() {
        editText.get()?.postDelayed({ editText.get()?.showKeyboard() }, 50)
    }
    fun hideKeyboard() {
        editText.get()?.postDelayed({ editText.get()?.hideKeyboard() }, 50)
    }
}

private fun EditText.hideKeyboard() {
    requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as
            InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

private fun EditText.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as
            InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

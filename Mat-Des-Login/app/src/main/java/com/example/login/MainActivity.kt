package com.example.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.example.login.FieldValidators.isStringContainNumber
import com.example.login.FieldValidators.isStringContainSpecialCharacter
import com.example.login.FieldValidators.isStringLowerAndUpperCase
import com.example.login.FieldValidators.isValidEmail
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupListeners()

        loginButton.setOnClickListener{
            if (isValidate()) {
                Toast.makeText(this, "validated", Toast.LENGTH_SHORT).show()
            }
        }
    }
    private fun isValidate(): Boolean = validateEmail() && validatePassword()

    private fun setupListeners() {
        email.addTextChangedListener(TextFieldValidation(email))
        password.addTextChangedListener(TextFieldValidation(password))
    }
    private fun validateEmail(): Boolean {
        if (email.text.toString().trim().isEmpty()) {
            emailTextInputLayout.error = "Required Field!"
            email.requestFocus()
            return false
        } else if (!isValidEmail(email.text.toString())) {
            emailTextInputLayout.error = "Invalid Email!"
            email.requestFocus()
            return false
        } else {
            emailTextInputLayout.isErrorEnabled = false
        }
        return true
    }

    private fun validatePassword(): Boolean {
        if (password.text.toString().trim().isEmpty()) {
            passwordTextInputLayout.error = "Required Field!"
            password.requestFocus()
            return false
        } else if (password.text.toString().length < 6) {
            passwordTextInputLayout.error = "password can't be less than 6"
            password.requestFocus()
            return false
        } else if (!isStringContainNumber(password.text.toString())) {
            passwordTextInputLayout.error = "Required at least 1 digit"
            password.requestFocus()
            return false
        } else if (!isStringLowerAndUpperCase(password.text.toString())) {
            passwordTextInputLayout.error =
                "Password must contain upper and lower case letters"
            password.requestFocus()
            return false
        } else if (!isStringContainSpecialCharacter(password.text.toString())) {
            passwordTextInputLayout.error = "1 special character required"
            password.requestFocus()
            return false
        } else {
            passwordTextInputLayout.isErrorEnabled = false
        }
        return true
    }
    inner class TextFieldValidation(private val view: View) : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            // checking ids of each text field and applying functions accordingly.
            when (view.id) {
                R.id.email -> {
                    validateEmail()
                }
                R.id.password -> {
                    validatePassword()
                }
            }
        }
    }
}
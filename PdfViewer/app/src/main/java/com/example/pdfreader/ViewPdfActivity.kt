package com.example.pdfreader

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.github.barteksc.pdfviewer.PDFView

class ViewPdfActivity : AppCompatActivity() {

    lateinit var pdfView: PDFView
    val PDF_SELECTION_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_pdf)

        pdfView = findViewById(R.id.pdfView)

        selectPdfFromStorage()
    }

    private fun selectPdfFromStorage() {
        Toast.makeText(this, "Selecciona archivo pdf", Toast.LENGTH_SHORT).show()
        val browerStorage = Intent(Intent.ACTION_GET_CONTENT)
        browerStorage.type = "application/pdf"
        browerStorage.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(Intent.createChooser(browerStorage,"Select Pdf"),PDF_SELECTION_CODE)

    }

    fun showPdfFromUri(uri: Uri){
        pdfView.fromUri(uri)
            .defaultPage(0)
            .spacing(10)
            .load()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==PDF_SELECTION_CODE && resultCode== Activity.RESULT_OK && data != null){
            val selectedPdf: Uri? = data.data
            selectedPdf?.let { showPdfFromUri(it) }

        }
    }
}
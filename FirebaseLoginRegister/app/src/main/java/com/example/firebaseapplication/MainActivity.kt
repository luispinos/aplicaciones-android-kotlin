package com.example.firebaseapplication

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    //Para cambiar la imagen de nuestro perfil
    private val fileResult = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        auth = Firebase.auth

        signOutImageView.setOnClickListener {
            signOut()
        }

        updateProfileAppCompatButton.setOnClickListener{
            updateProfile(nameEditText.text.toString())
        }

        //Para cambiar la imagen de nuestro perfil
        profileImageView.setOnClickListener {
            fileManager()
        }

        updatePasswordTextView.setOnClickListener {
            startActivity(Intent(this,UpdatePasswordActivity::class.java))
        }
        deleteAccountTextView.setOnClickListener {
            startActivity(Intent(this,DeleteAccountActivity::class.java))
        }

        updateUI()
    }

    //Actualizar los datos del usuario
    private fun updateProfile(name : String){
        val user = auth.currentUser
        val profileUpdates = userProfileChangeRequest {
            displayName = name
        }
        user!!.updateProfile(profileUpdates)
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                    Toast.makeText(this, "Se realizaroh los cambios correctamente", Toast.LENGTH_SHORT).show()
                    updateUI()
                }
            }
    }

    //Para cambiar nuestro imagen
    private fun fileManager() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, fileResult)
    }
    //Para cambiar nuestro imagen
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == fileResult) {
            if (resultCode == RESULT_OK && data != null) {
                val uri = data.data

                uri?.let { imageUpload(it) }

            }
        }
    }

    //Para cambiar nuestro imagen
    private fun imageUpload(mUri: Uri) {

        val user = auth.currentUser
        val folder: StorageReference = FirebaseStorage.getInstance().reference.child("Users")
        val fileName: StorageReference = folder.child("img"+user!!.uid)

        fileName.putFile(mUri).addOnSuccessListener {
            fileName.downloadUrl.addOnSuccessListener { uri ->

                val profileUpdates = userProfileChangeRequest {
                    photoUri = Uri.parse(uri.toString())
                }

                user.updateProfile(profileUpdates)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(this, "Se realizaron los cambios correctamente.",
                                Toast.LENGTH_SHORT).show()
                            updateUI()
                        }
                    }
            }
        }.addOnFailureListener {
            Log.i("TAG", "file upload error")
        }
    }

    private fun signOut(){
        Firebase.auth.signOut()
        val intent = Intent(this,LoginActivity::class.java)
        startActivity(intent)
    }

    //Recuperaremos nuestro usuario
    private fun updateUI(){
        val user = auth.currentUser

        if (user!=null){
            emailTextView.text = user.email

            if (user.displayName !=null){
                nameTextView.text = user.displayName
                nameEditText.setText(user.displayName)
            }

            //Para mostrar imagenes tenemos que usar la libreria de Glade
            Glide
                .with(this)
                .load(user.photoUrl)
                .centerCrop()
                .placeholder(R.drawable.profile_photo)
                .into(profileImageView)
            Glide
                .with(this)
                .load(user.photoUrl)
                .centerCrop()
                .placeholder(R.drawable.profile_photo)
                .into(bgProfileImageView)

        }
    }
}
package com.example.firebaseapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_account_recovery.*


class AccountRecoveryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_recovery)

        senEmailAppCompatButton.setOnClickListener {
            val emailAddress = emailEditText.text.toString()
            Firebase.auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener {task ->
                    if (task.isSuccessful){
                        startActivity(Intent(this,LoginActivity::class.java))
                    }else{
                        Toast.makeText(this, "La cuenta $emailAddress NO ESTA REGISTRADA en la aplicación", Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }
}
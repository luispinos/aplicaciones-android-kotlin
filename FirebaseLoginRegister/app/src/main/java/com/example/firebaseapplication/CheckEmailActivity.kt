package com.example.firebaseapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_check_email.*

class CheckEmailActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_email)

        auth = Firebase.auth

        val user = auth.currentUser

        veficateEmailAppCompatButton.setOnClickListener {
            val profileUpdates = userProfileChangeRequest {

            }
            user!!.updateProfile(profileUpdates)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        if(user.isEmailVerified){
                            startActivity(Intent(this,MainActivity::class.java))
                        }else{
                            Toast.makeText(this, "Verifique su correo", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
        }

        signOutImageView.setOnClickListener{
            signOut()
        }
    }

    //Esto se pone para que la aplicacion se quede abierto y no tenga que cerrarse link: https://firebase.google.com/docs/auth/android/start?hl=es-419
    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if(currentUser != null){
            if (currentUser.isEmailVerified){
                reload()
            } else{
                sendEmailVerificacion()
            }
        }
    }

    private fun sendEmailVerificacion(){
       val user = auth.currentUser
       user!!.sendEmailVerification().addOnCompleteListener (this){ task ->
           if (task.isSuccessful){
               Toast.makeText(this, "Se envio un correo de vereficacion", Toast.LENGTH_SHORT).show()
           }
       }
    }

    //Y esta funcion igual
    private fun reload() {
        startActivity(Intent(this,MainActivity::class.java))
    }

    private fun signOut(){
        Firebase.auth.signOut()
        startActivity(Intent(this,LoginActivity::class.java))

    }
}
package com.example.firebaseapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import android.util.Log
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class LoginActivity : AppCompatActivity() {

    //El lateinit es que los vamos a utilizar después
    private lateinit var layoutLoginEmail:TextInputLayout
    private lateinit var layoutLoginPassword:TextInputLayout
    private lateinit var loginEmail:TextInputEditText
    private lateinit var loginPassword:TextInputEditText
    private lateinit var loginButton: Button
    private lateinit var registerButton: Button
    private lateinit var forgotPassword: TextView
    //Instancia de firebase auth que hemos implemetnado en el build gradle
    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //Inicializamos la variable de firebaseAuth y demas
        auth = Firebase.auth
        //Con estos dos de abajo tam,bién podemos utilizar el string de los Editext de Material Design
        layoutLoginEmail = findViewById(R.id.loginEmail)
        layoutLoginPassword = findViewById(R.id.passwordTextInputLayout)
        loginEmail = findViewById(R.id.email)
        loginPassword = findViewById(R.id.password)
        loginButton = findViewById(R.id.loginButton)
        registerButton = findViewById(R.id.registerButton)
        forgotPassword = findViewById(R.id.txt_ForgotPassword)

        loginButton.setOnClickListener{
            val email = loginEmail.text.toString()
            val password = loginPassword.text.toString()


            when {
                password.isEmpty() || email.isEmpty()-> {
                    layoutLoginEmail.error = "Campo requerido"
                    loginEmail.requestFocus()
                    layoutLoginPassword.error = "Campo requerido"
                    loginPassword.requestFocus()
                    Toast.makeText(this, "Email o contraseña o incorrectos.",
                        Toast.LENGTH_SHORT).show()
                }
                else -> {
                    login(email, password)
                }
            }
            //if(checkEmpty(email,password)){
                //login(email,password)
            //}

        }
        registerButton.setOnClickListener {
            startActivity(Intent(this,RegisterActivity::class.java))
            finish()
        }

        txt_ForgotPassword.setOnClickListener {
            startActivity(Intent(this,AccountRecoveryActivity::class.java))
        }
    }

    private fun login(email:String,password:String){
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful){
                    Log.d("TAG", "signInWithEmail:success")
                    reload()
                    finish()
                }else{
                    Log.w("TAG", "signInWithEmail:failure", task.exception)
                    Toast.makeText(applicationContext,"Inicio de sesión fallido, comprueba que \n el correo sea y la contraseña sean correctas", Toast.LENGTH_LONG).show()
                }
            }
    }

    //Esto se pone para que la aplicacion se quede abierto y no tenga que cerrarse link: https://firebase.google.com/docs/auth/android/start?hl=es-419
    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if(currentUser != null){
            if (currentUser.isEmailVerified){
                reload();
            }else{
                startActivity(Intent(this,CheckEmailActivity::class.java))
            }

        }
    }

    //Y esta funcion igual
    private fun reload() {
        startActivity(Intent(this,MainActivity::class.java))
    }
}
package com.example.cursokotlinprincipiantes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    val TAG =":::TAG"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //val personData = DataClases("Luis Pinos",26)
        //val person1 = Person(personData)
        //person1.presentacion()
        //val person2 = SecondPersonInterface(personData)
        //person2.presentacion()
        //val edad = person2.returnAge(1996)
        //Log.d(TAG,"Los años son $edad")
        //val programador = Programador()
        //val result = programador.getProgrammerData()
        //Log.d(TAG,"My name is ${result.name} and my favorite language is ${result.language} and i have ${result.age} years")
        //val firstValue = 10
        //val secondValue = 15
        //val resultado = (firstValue - secondValue) <= (secondValue - firstValue)
        //Log.d(TAG,"El resultado es $resultado") //True
        //ifs()
        //condicionalesWhen()
        //listados()
        //fores()
        //whiles()
        //dowhiles()
        botDeSeguridad()
    }

    //Aqui vamos a ver variables y constantes
    private fun variablesYConstantes(){
        /*
        Es un nombre para representar el valor de un dato
         */

        //Comentario en Kotlin
        var myFirstVariable ="Hola Mundo no se puede cambiar de texto a otro valor en var normales!!"
        val myFirstConstant = "Seria mi primera constante y no puede ser modificada porque da error \n tampoco"
        /*
        Si ponemos solo el de arriba y damos a enter te lo cierra automaticamente cosa que
        no lo sabia.
         */

        //Para ver algo por consola es recomendable los logs hay que pasarle dos parametros

        Log.d(TAG,myFirstVariable)
        Log.d(TAG,myFirstConstant)
    }
    private fun tiposDeDatos(){
        //Cadenas de texto
        var string ="My String"
        //Valores numericos enteros(int,short,long, byte)
        var numberr: Long =10
        var number = 10
        //Valores numericos decimales(double, float)
        var decimal: Double = 10.5
        var decimalFloat: Float = 12.6f
        //Si por ejemplo se pone o se inteta modificar la variable number Long con un decimal da error.
        //Condiciones verdades o falsas
        var boolean = false
        var boolean2 = true

        //Log solo permite datos de typpo string, se puede utilizar .toString o $nombrevariable y dentro de ""
        //ejem: "My number is $number"
    }
    private fun operadores(){
        val firstVale = 5.0
        val secondValue = 2.0

        //Operadpres aritmeticos
        val suma = firstVale + secondValue
        val resta = firstVale - secondValue
        val mul = firstVale*secondValue
        val div = firstVale/secondValue
        val rest = firstVale%secondValue
        Log.d(TAG, "El resultado es $suma ")
        Log.d(TAG, "El resultado es $resta ")
        Log.d(TAG, "El resultado es $mul ")
        Log.d(TAG, "El resultado es $div ")
        Log.d(TAG, "El resultado es $rest ")

        //Operadores de comparación tienen que ser los dos del mismo typo int o boolean
        val igualdad = firstVale == secondValue
        val desigualdad = firstVale != secondValue
        val menor = firstVale < secondValue
        val mayor = firstVale > secondValue

        Log.d(TAG, "El resultado es $igualdad ")
        Log.d(TAG, "El resultado es $desigualdad ")
        Log.d(TAG, "El resultado es $menor ")
        Log.d(TAG, "El resultado es $mayor ")

        //Operadores Logicos queremos comparar si se cumplen condiciones de true o false
        val conjucion = igualdad && menor //Si esto se cumpliera da true si no da false

        val disyuncion = igualdad || menor //Si alguna de las dos se cumple devuelve true
        val negacion = !igualdad //Si es distinto de true
    }
    private fun nullSafety(){
        //Una variable string no puede ser nula pero si ponemos una ? le estamos diciendo que puede que sea nula pero no a 100%
        //Aqui vamos a evitar los errores de nullpointerException que suele ocurrir
        val nullString:String? = null
        //Si lo dejamos asi nunca se ejecutaria por siempre va ser nulo
        if(nullString != null){
            Log.d(TAG,nullString)
        }

        //Hay una forma de hacerlo en una sola linea desde Kotlin y es asi
        nullString?.let { Log.d(TAG,nullString) }?: run { Log.d(TAG,nullString!!) } //Con doble !! le decimos que no se preocupe
        //que la variable no es nula
        //Solo se va a ejecutar si la variable nullString no es nula
        // Pero si lo es ejecutamos este segundo apartado
    }
    private fun funciones(){
        //Hya funciones simples, dinamicas que son que se utilizan varias veces y por parametros
        //Funcion de retorno
        val fechaNacimiento = funcionRetorno(2022,24)
        Log.d(TAG,"La fecha de nacimiento es $fechaNacimiento")
    }
    //Funcion con retorno de valor, detras de los parentesis hay que poner que quieren que nos devuelva
    private fun funcionRetorno(fechaActual: Int, age: Int): Int {
        return fechaActual - age
        //Esto se puede hacer mas compacto todavia si solo tiene que hacer una cosa como en este caso ej abajo
        //private fun funcionRetorno(fechaActual: Int, age: Int): Int = fechaActual - age
    }
    //Una interfaz es uncontrato que las clases lo usan para querer implementarla obligatoriamente.
    //Ademas los ifs se pueden usar para dar valor a una variable ejl:
    private fun ifs(){
        val booleanValue = false
        val edad: Int = if(!booleanValue){
            17
        }else{
            26
        }
        Log.d(TAG,edad.toString())
    }
    //COndicionales con When
    private fun condicionalesWhen(){
        val language = "Python"

        when(language){
            //Si queremos que tanto scala como kotlin ejecuten el mismo metodo se hace asi
            "Kotlin", "Scala" -> Log.d(TAG,"Lenguaje Seleccionado Kotlin")
            "Java" -> Log.d(TAG,"Lenguaje Seleccionado Java")
            "Python" -> { //Esto se pone cuando se quiere hacer mas condiciones dentro de un caso ifs y demas
                Log.d(TAG,"Lenguaje Seleccionado Pyhton")}
            "Ruby" -> Log.d(TAG,"Lenguaje Seleccionado Ruby") //El finally de switch aqui es else
            else -> Log.d(TAG,"Lenguaje No valido")
        }

        //Con numeros
        val myNumber = 9

        when(myNumber){
            //Si queremos un rango de numeros desde 0 a 10
            in 0..10 -> Log.d(TAG,"Lenguaje Seleccionado Kotlin")
            40 -> Log.d(TAG,"Lenguaje Seleccionado Java")
            in 80..119 -> { //Esto se pone cuando se quiere hacer mas condiciones dentro de un caso ifs y demas
                Log.d(TAG,"Lenguaje Seleccionado Pyhton")}
            120 -> Log.d(TAG,"Lenguaje Seleccionado Ruby") //El finally de switch aqui es else
            else -> Log.d(TAG,"Lenguaje No valido")
        }
    }
    private fun listados(){
        //Conjunto de datos de un mismo tipo que estan ordenados no haria falta string en este caso porque ya lo detecta
        val list = listOf<String>("Rodrigo","Macarena","Ruben","Susana")
        val listt : List<String> = listOf()

        //Array otro tipo de listado
        val arrayList = arrayListOf<String>()
        val arrayListt: ArrayList<String> = arrayListOf("Rodrigo","Macarena","Ruben","Susana")

        //La diferencia es que un listado es inmutable , es lo que es en la creacion, un arraylist si es mutable
        //para eliminar un apartado en un arrylist usariamos el .remove en string para eliminar por posicion se
        //utilizaria el removeAt
        arrayListt.remove("Macarena")
        arrayListt.addAll(list)
        Log.d(TAG,arrayListt.toString())
    }
    private fun fores(){
        val arrayListt: ArrayList<String> = arrayListOf("Rodrigo","Macarena","Ruben","Susana","Juan Tenorio")

        for (persona in arrayListt){
            Log.d(TAG,persona)
        }
        //Con numeros si en vez de .. ponemos until se omite el ultimo numero si queremos que el recorrido
        //vala de 2 en 2 se pone despues del 15 step 2 para recorrer hacia abajo se utiliza downTo
        for (position in 0..15){
            Log.d(TAG,position.toString() )
        }
    }
    private fun whiles(){
        var myNumber = 0
        while (myNumber<=10){
            Log.d(TAG,myNumber.toString())
            myNumber += 3
        }
    }
    private fun dowhiles(){
        var myNumber = 0

        do {
            Log.d(TAG,myNumber.toString())
            myNumber++;
        }while (myNumber<=10)
    }
    private fun botDeSeguridad(){
        val persona = DataClases("Luis Pinos",15, arrayListOf("Programar","TV","Deportes"))

        if (persona.name.equals("Luis Pinos")){
            Log.d(TAG,"Bienvenido ${persona.name}")
            when(persona.age){
                in 0..17 -> {
                    Log.d(TAG,"Tienes ${persona.age} años, eres demasiado joven para acceder")
                }in 18..64 -> {
                    Log.d(TAG,"Tienes ${persona.age} años, puedes acceder")

                    for (hobby in persona.hobbies){
                        Log.d(TAG,"Te gusta $hobby")
                    }
                }in 65..100 -> {
                    Log.d(TAG,"Tienes ${persona.age} años, eres demasiado mayor para acceder")
                }
            }
        }else{
            Log.d(TAG,"Nombre incorrecto ${persona.name}")
        }
    }
}
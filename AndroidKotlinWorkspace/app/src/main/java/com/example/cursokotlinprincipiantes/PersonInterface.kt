package com.example.cursokotlinprincipiantes

interface PersonInterface {
    //Las funciones que creemos aqui y en la clase donde la estemos implementando van a hacer obligatorias
    fun returnAge(birthYear:Int):Int
}
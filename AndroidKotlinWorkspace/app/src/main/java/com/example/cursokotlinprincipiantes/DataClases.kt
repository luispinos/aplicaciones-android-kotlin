package com.example.cursokotlinprincipiantes

    //Clase que se usa para el almacenamiento de datos. Aqui no hay que poner los { ya que es algo para almacenar
    data class DataClases (
        val name:String?,//Practicamos con los valores nulos para decirle que el nobre podria ser nulo es decir que podriamos no pasarle nombre
        val age: Int, //Hay que tener cuidado porque si se pone en privado estas variables no se pueden acceder
        val hobbies: ArrayList<String>
    )
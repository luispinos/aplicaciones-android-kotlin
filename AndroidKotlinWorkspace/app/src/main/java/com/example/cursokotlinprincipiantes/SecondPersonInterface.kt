package com.example.cursokotlinprincipiantes

import android.util.Log

//Con los dos puntos esta extendiendo de person interface o heredando
class SecondPersonInterface(private val data: DataClases): PersonInterface {
    fun presentacion(){
        Log.d(":::TAG","My name is ${data.name} and I have ${data.age} years")
    }

    override fun returnAge(birthYear: Int): Int = 2022 - birthYear
}
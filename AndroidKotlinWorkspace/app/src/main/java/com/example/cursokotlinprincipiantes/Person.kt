package com.example.cursokotlinprincipiantes

import android.util.Log

//A las clases también le podemos pasar parametros pero aqui hay que poner val,var no como en las funciones
class Person (private val data: DataClases){

    val TAG = ":::TAG"

    fun presentacion(){
        Log.d(TAG,"My name is ${data.name} and my age is ${data.age}")
    }
}
package com.example.cursokotlinprincipiantes

class Programador : ProgrammerInterface{

    override fun getProgrammerData(): ProgrammerData = ProgrammerData(getName(),getAge(),getLanguage())

    private fun getAge():Int{
        return 24
    }
    private fun getName():String{
        return "Luis Pinos"
    }
    private fun getLanguage():String{
        return "Kotlin Android"
    }

}


data class ProgrammerData(
    val name: String,
    val age: Int,
    val language: String
)

interface ProgrammerInterface{
    fun getProgrammerData(): ProgrammerData
}
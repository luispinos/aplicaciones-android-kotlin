package com.example.aplicacionlapb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        var emaile = edt_email.text
        var password = edt_password.text

        btn_login.setOnClickListener {
            Toast.makeText(this, "Email: $emaile", Toast.LENGTH_SHORT).show()
            Toast.makeText(this, "Password : $password", Toast.LENGTH_LONG).show()
        }

        txt_crearCuenta.setOnClickListener {
            startActivity(Intent(this,Register::class.java))
        }



    }
}
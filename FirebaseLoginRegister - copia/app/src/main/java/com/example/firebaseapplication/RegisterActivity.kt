package com.example.firebaseapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class RegisterActivity : AppCompatActivity() {

    private lateinit var registerEmail: EditText
    private lateinit var registerPassword: EditText
    private lateinit var registerRepeatPassword: EditText
    private lateinit var registerButtonRegister: Button
    private lateinit var goLogginButton: Button

    //Instancia de firebase auth que hemos implemetnado en el build gradle
    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        //Inicializamos la variable de firebaseAuth y demas
        auth = Firebase.auth
        registerEmail = findViewById(R.id.registerEmail)
        registerPassword = findViewById(R.id.registerPassword)
        registerRepeatPassword = findViewById(R.id.registerRepeatPassword)
        registerButtonRegister = findViewById(R.id.registerButtonRegister)
        goLogginButton = findViewById(R.id.goLogginButton)

        goLogginButton.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java))
            //Para que no se quede ocupando espacio la tarea le tenemos que poner finish
            finish()
        }
        registerButtonRegister.setOnClickListener {
            val email = registerEmail.text.toString()
            val password = registerPassword.text.toString()
            val repeatPassword = registerRepeatPassword.text.toString()

            if(password.equals(repeatPassword) && checkEmpty(email,password,repeatPassword)){
                register(email,password)
            }
        }
    }

    private fun register(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener(this){task ->
                if (task.isSuccessful){
                    startActivity(Intent(this,MainActivity::class.java))
                    //Para que no se quede ocupando espacio o memoria la tarea le tenemos que poner finish
                    finish()
                }else{
                    Toast.makeText(applicationContext,"Register failed",Toast.LENGTH_LONG).show()
                }

            }
    }

    private fun checkEmpty(email: String, password: String, repeatPassword: String): Boolean {
        return email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()
    }
}
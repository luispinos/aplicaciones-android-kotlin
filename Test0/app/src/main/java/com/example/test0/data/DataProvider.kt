package com.example.test0.data

class DataProvider {
    val dogList = listOf(
        Dog(
            name = "Bruta",
            breed = "Bulldog",
            years = 1,
            image = "https://images.dog.ceo/breeds/bulldog-boston/n02096585_1761.jpg"
        ),
        Dog(
            name = "Jack",
            breed = "Bulldog",
            years = 1,
            image = "https://images.dog.ceo/breeds/bulldog-boston/n02096585_1761.jpg"

        ),
        Dog(
            name = "Kala",
            breed = "Bulldog",
            years = 1,
            image = "https://images.dog.ceo/breeds/bulldog-boston/n02096585_1761.jpg"
        ),
        Dog(
            name = "Marea",
            breed = "Bulldog",
            years = 1,
            image = "https://images.dog.ceo/breeds/bulldog-boston/n02096585_1761.jpg"
        ),
        Dog(
            name = "Lukita",
            breed = "Bulldog",
            years = 1,
            image = "https://images.dog.ceo/breeds/bulldog-boston/n02096585_1761.jpg"
        )
    )
}
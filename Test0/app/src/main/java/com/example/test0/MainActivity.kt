package com.example.test0

import android.graphics.Paint
import android.os.Bundle
import android.os.Message
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.test0.data.DataProvider
import com.example.test0.ui.theme.Test0Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DogList(dogs = DataProvider.dogList)
            //MainScreen()
            //GreetingText("Culo")
        }
    }
}

//Componente padre
//Aqui le pasamos el contrustor del viewmodel y lo vamos a inicializar por defecto
@Composable
fun MainScreen(viewModel: MainViewModel = MainViewModel()){
    //Cardview
    CardItem()
    /*
    //Ultimo apartado de Listas
    MessageList(messages = listOf("Hola","aDIOS"))
     */
    /*
    //Ahora que lo controlamos el estado desde el view model la linea de abajo nos la cargamos
    //El apartado de abajo no me funciono
    //val nameState = viewModel.textFieldState.observeAsS()
    //LiveData y ViewModel
    val nameState = remember {
        mutableStateOf("")
    }
    Surface(
        color = Color.LightGray,
        modifier = Modifier.fillMaxSize()
    ){
        MainLayout(name = nameState.value){
            //Recogeremos el callback para actualizar
            newName -> nameState.value = newName
        }
    }*/
    /*
    //State Hosting
    val students = remember {
        mutableStateListOf("Luis","Alberto")
    }
    //Esto es un estado y tiene que ir enlazado con REMEMBER
    val newStudentName = remember {
        //En este caso dice que lo va a intentar en un estado vacio
        mutableStateOf("")
    }
    Surface(
        color = Color.LightGray,
        modifier = Modifier.fillMaxSize()
    ){
        //State Hosting
        StudentList(students,
        { students.add(newStudentName.value) },
            newStudentName.value,
            //Nueva lamba que va a cambiar cada vez que textfield vuelva a escribir
            {
                //Que ese value esa actualizado
                newStudent -> newStudentName.value = newStudent
            }
        )
        /*
        Row {

            MySquare(name = Color.Blue)
            MySquare(name = Color.Green)

            /*
        androidx.compose.material.Surface(
            color = Color.Green,
            modifier = Modifier.wrapContentSize()
        ) {
            //Surface solo acepta 1 hijo es decir un componente mas
            Text(text = "Hello Lucas!",
                modifier = Modifier.wrapContentSize(),
                style = MaterialTheme.typography.h5,
                fontWeight = FontWeight.Light)
        }*/
        }*/

    }*/
}//Cierre Main
@Composable
fun CardItem() {
    Card(
        Modifier
            .padding(10.dp)
            .fillMaxWidth(),
        elevation = 10.dp
    ) {
        Column(
            Modifier.padding(10.dp)
        ) {
            Text(text = "Hello OpenWebinars")
            Text(text = "This is a card test")
        }
    }
}
/*
@Composable
//CardItem
fun CardItem() {
    Card(
        Modifier
            .padding(10.dp)
            .fillMaxWidth(),
        elevation = 10.dp
    ) {
        Column(
            Modifier.padding(10.dp)
        ) {
            Text(text = "Hello OpenWebinars")
            Text(text = "This is a card test")
        }
    }
}
 */


//Forma sencilla de hacer una lista comparada con los recyclerview los adapter, los xml
@Composable
fun MessageList(messages: List<String>){
    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ){
        items(messages){
            message -> MessageInfo(message = message)
        }
    }
}
@Composable
fun MessageInfo(message: String){
    Text(text = message)
}


//LiveData y ViewModel Una vez terminado esto vamos a elevar esto fuera de nuestro mainActivity a taves
//del componente viewModel
@Composable
fun MainLayout(
    name: String,
    onTextFieldChange: (String) -> Unit
){
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        TextField(value = name , onValueChange = onTextFieldChange )
        //Faltaba este paso paaso porque es la entrada de texto que se va a actualizar
        Text(
            text = name
        )
    }
}

//State Hosting
@Composable
fun StudentList(students: List<String>,
                onButtonClick:() -> Unit,
                studentName: String,
                //Creamos una lamba para que nos devuelva el dato Textfield
                onStudentNameChanged: (String) -> Unit
){
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        for (student in students){
            Text(text = student)
        }
        //TextField con State este componenete solo se va aencargagr de pintar datos
        TextField(value = studentName,
            onValueChange = onStudentNameChanged
        )
        //State Hosting
        Button(onClick = onButtonClick) {
            Text(text = "Añadir")
        }
        /*
        Button(onClick = { students.add("Jaime") }) {
            Text(text = "Añadir")
        }*/
    }
}
//Compose nos da muchas utilizades para hacer componentes custom
@Composable
fun MySquare(name: Color){
    Surface(
        color = name,
        modifier = Modifier.size(60.dp)
    ) {}
}

/*
@Composable
fun GreetingText(name: String) {
    Text(text = "Hello $name!",
    modifier = Modifier
        .width(80.dp)
        .height(220.dp)
        .background(Color.Yellow),
    style = /*TextStyle(
        color = Color.Red,
        fontWeight = FontWeight.Bold,)*/
    MaterialTheme.typography.button,
        fontWeight = FontWeight.Light
    )
}*/


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DogList(dogs = DataProvider.dogList)
}
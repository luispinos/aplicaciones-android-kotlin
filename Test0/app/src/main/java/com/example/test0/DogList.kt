package com.example.test0

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import com.example.test0.data.Dog

@Composable
fun DogList(dogs:List<Dog>){
    LazyColumn(
        contentPadding = PaddingValues(horizontal = 16.dp,)),
        verticalArragement = Arragement.spaceBy(8.dp){
            items(dogs){
                dog -> DogItem(dog = dog)
            }
        }
}
package com.example.test0.data

class Dog(
    val name: String,
    val breed: String,
    val years: Int,
    val image: String
    )

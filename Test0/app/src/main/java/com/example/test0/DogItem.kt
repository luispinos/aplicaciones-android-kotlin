package com.example.test0

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.unit.dp
import com.example.test0.data.Dog
import coil.compose.*


@Composable
fun DogItem(dog: Dog) {
    Card(
        Modifier.fillMaxWidth(),
        elevation = 10.dp
    ) {
        Row(
            Modifier.fillMaxWidth()
        ) {
            Image(painter = remeberImagePainter(dog.image),
                contentDescription = "")
        }
    }
}
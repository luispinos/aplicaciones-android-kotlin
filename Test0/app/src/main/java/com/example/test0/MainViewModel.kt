package com.example.test0

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel:ViewModel() {
    //definir una property que sea el estado que va ir variando q sera inicializado en un estado vacio
    val textFieldState = MutableLiveData("")

    fun onTextField(newText: String){
        //Ahora tenenmos que ir a mainAc que es la parte visual desde donde lo trataremos
        textFieldState.value = newText
    }
}